package com.abelhernandez.lectorqr;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private static final int SCANNER_ID = 0;
    private static final int PERMISSION_CODE = 100;
    private Button buttonLector;
    private TextView textViewResult1,textViewResult2;
    private IntentIntegrator intentScanner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindUI();
        getButtonLector().setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.am_btn_lector:
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    checkPermissionsCamera();
                } else {
                    setIntentScanner();
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setIntentScanner();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SCANNER_ID:
                IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
                if(intentResult.getContents() != null) {
                    String result = intentResult.getContents();
                    String[] items = result.split(",");
                    if(items.length > 0) {
                        getTextViewResult1().setText(items[0]);
                        getTextViewResult2().setText(items[1]);
                    } else {
                        Toast.makeText(this,"Codigo incorrecto",Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    private void bindUI() {
        buttonLector = findViewById(R.id.am_btn_lector);
        textViewResult1 = findViewById(R.id.am_tv_result1);
        textViewResult2 = findViewById(R.id.am_tv_result2);
        intentScanner               =   new IntentIntegrator(this);
    }

    private void setIntentScanner() {
        getIntentScanner().setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        getIntentScanner().setPrompt("Lector QR");
        getIntentScanner().setCameraId(SCANNER_ID);
        getIntentScanner().setBeepEnabled(false);
        getIntentScanner().setBarcodeImageEnabled(true);
        getIntentScanner().initiateScan();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissionsCamera() {
        requestPermissions(new String[]{android.Manifest.permission.CAMERA},PERMISSION_CODE);
    }






    public Button getButtonLector() {
        return buttonLector;
    }

    public void setButtonLector(Button buttonLector) {
        this.buttonLector = buttonLector;
    }

    public TextView getTextViewResult1() {
        return textViewResult1;
    }

    public void setTextViewResult1(TextView textViewResult1) {
        this.textViewResult1 = textViewResult1;
    }

    public TextView getTextViewResult2() {
        return textViewResult2;
    }

    public void setTextViewResult2(TextView textViewResult2) {
        this.textViewResult2 = textViewResult2;
    }

    public IntentIntegrator getIntentScanner() {
        return intentScanner;
    }
}
